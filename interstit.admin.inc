<?php

/**
 * @file
 * Intersit administration functions.
 */

/**
 * Form constructor for Interstit settings.
 */
function interstit_admin_form($form, &$form_state) {

	$form['interstit_state'] = array(
			'#type' => 'checkbox',
			'#title' => t('Enable'),
			'#default_value' => variable_get('interstit_state', 0)
	);

	$form['interstit_front'] = array(
			'#type' => 'checkbox',
			'#title' => t('Only on frontpage'),
			'#default_value' => variable_get('interstit_front', 1)
	);

	$form['interstit_test'] = array(
			'#type' => 'checkbox',
			'#title' => t('Test mode (no cookie check)'),
			'#default_value' => variable_get('interstit_test', 0)
	);

	$form['interstit_mode'] = array(
			'#type' => 'radios',
			'#title' => t('Mode'),
			'#options' => array(
					0 => t('JS overlay'),
					1 => t('Page redirect')
			),
			'#default_value' => variable_get('interstit_mode', 0),
	);

	$form['interstit_cookiename'] = array(
			'#type' => 'textfield',
			'#title' => t('Cookie name'),
			'#size' => 16,
			'#required' => true,
			'#default_value' => variable_get('interstit_cookiename', 'drinterstit_'.time()),
	);

	/*$form['interstit_clear'] = array(
	 '#type' => 'checkbox',
			'#title' => t('Clear this cookie'),
			'#default_value' => 0
	);*/

	$form['interstit_cookielife'] = array(
			'#type' => 'textfield',
			'#title' => t('Cookie lifetime'),
			'#size' => 4,
			'#required' => true,
			'#field_suffix' => t('hours'),
			'#default_value' => variable_get('interstit_cookielife', 1),
	);

	$form['interstit_content'] = array(
			'#type' => 'textarea',
			'#title' => t('Interstitial advert HTML content'),
			'#size' => 6,
			'#required' => true,
			'#default_value' => variable_get('interstit_content', ''),
			'#description' => t("If not empty, the size of all text fields in compact forms will be set to the entered size."),
	);

	return system_settings_form($form);
}

