(function($) {
	
	Drupal.behaviors.interstit = {
		attach: function(context, settings) {
			Drupal.intads.setContent(settings.interstit.key);
			Drupal.intads.doLayer();
		}
	}
	
	Drupal.intads = (function(module) {
	  var cont = '';
	  module = {
	    'init': function() {
	    },
	    'setContent': function(content) {
	    	this.cont = content;
	    },
	    'getContent': function() {
	        return this.cont;
	    },
	    'doLayer': function() {
		var content = this.getContent();
		$('body').append("<div class='container' id='interstitWrapper'>"+content+"</div>");
		$('#interstitWrapper').modal({
		  
		    'autoposition': true,
		    'autoresize': true
		  
		});
	    }
	  }
	  return module;
	})(Drupal.intads || {});
	
})(jQuery);